import logging
import time
from typing import Dict, List, Optional, Tuple
from osm2city.utils.osm_db_provider import OsmDBProvider
from osm2city.utils.osmparser import OSMReadResult, Node, Way, Member, Relation
from osm2city import parameters

import psycopg2


class OSMDbPostgresProvider( OsmDBProvider ):
    def __init__(self) -> None:
        pass

    def _fetch_osm_db_data_ways(self, required: List[str], is_key_values: bool = False) -> OSMReadResult:
        """Given a list of required keys or key/value pairs get the ways plus the linked nodes from an OSM database."""

        #FIXME: required is List[tuple]
        # rebuild like this: 'place=>{}'.format(member.name)
        start_time = time.time()

        db_connection = self.make_db_connection()
        if is_key_values:
            ways_dict = self.fetch_db_way_data(list(), required, db_connection)
            nodes_dict = self.fetch_db_nodes_for_way(list(), required, db_connection)
        else:
            ways_dict = self.fetch_db_way_data(required, list(), db_connection)
            nodes_dict = self.fetch_db_nodes_for_way(required, list(), db_connection)
        db_connection.close()

        logging.info("Reading OSM way data for {0!s} from db took {1:.4f} seconds.".format(required,
                                                                                        time.time() - start_time))
        return OSMReadResult(nodes_dict=nodes_dict, ways_dict=ways_dict,
                            relations_dict=None, rel_nodes_dict=None, rel_ways_dict=None)


    def fetch_osm_db_data_ways_key_values(self, req_key_values: List[tuple]) -> OSMReadResult:
        """Given a list of required key/value pairs get the ways plus the linked nodes from an OSM database."""
        req_key_values = [ "=>".join(kv) for kv in req_key_values ]
        return self._fetch_osm_db_data_ways(req_key_values, True)


    def fetch_osm_db_data_ways_keys(self, req_keys: List[str]) -> OSMReadResult:
        """Given a list of required keys get the ways plus the linked nodes from an OSM database."""
        return self._fetch_osm_db_data_ways(req_keys, False)


    def fetch_osm_db_data_relations_keys(self, input_read_result: OSMReadResult, first_part: str,
                                        relation_debug_string: str) -> OSMReadResult:
        """Updates an OSMReadResult with relation data based on required keys"""
        start_time = time.time()

        db_connection = self.make_db_connection()

        # common subquery
        sub_query = first_part + " AND r.id = rm.relation_id"
        sub_query += " AND rm.member_type = 'W'"
        sub_query += " AND rm.member_id = w.id"
        sub_query += " AND "
        sub_query += self.construct_intersect_bbox_query()

        # == Relations and members and ways
        # Getting related way data might add a bit of  volume, but reduces number of queries and might be seldom that
        # same way is in different relations for buildings.
        query = """SELECT r.id, r.tags, rm.member_id, rm.member_role, w.nodes, w.tags
        FROM relations AS r, relation_members AS rm, ways AS w
        WHERE
        """
        query += sub_query
        query += " ORDER BY rm.relation_id, rm.sequence_id"
        query += ";"

        result_tuples = self.fetch_all_query_into_tuple(query, db_connection)

        relations_dict = dict()
        rel_ways_dict = dict()

        for result in result_tuples:
            relation_id = result[0]
            member_id = result[2]
            if relation_id not in relations_dict:
                relation = Relation(relation_id)
                relation.tags = self._parse_hstore_tags(result[1], relation_id)
                relations_dict[relation_id] = relation
            else:
                relation = relations_dict[relation_id]

            my_member = Member(member_id, "way", result[3])
            relation.add_member(my_member)

            if member_id not in rel_ways_dict:
                my_way = Way(member_id)
                my_way.refs = result[4]
                my_way.tags = self._parse_hstore_tags(result[5], my_way.osm_id)
                rel_ways_dict[my_way.osm_id] = my_way

        # == Nodes for the ways
        query = """SELECT n.id, ST_X(n.geom) as lon, ST_Y(n.geom) as lat
        FROM relations AS r, relation_members AS rm, ways AS w, way_nodes AS wn, nodes AS n
        WHERE
        """
        query += sub_query
        query += " AND wn.way_id = w.id"
        query += " AND wn.node_id = n.id"
        query += ";"

        result_tuples = self.fetch_all_query_into_tuple(query, db_connection)

        rel_nodes_dict = dict()
        for result in result_tuples:
            my_node = Node(result[0], result[2], result[1])
            rel_nodes_dict[my_node.osm_id] = my_node

        logging.info("Reading OSM relation data for {0!s} from db took {1:.4f} seconds.".format(relation_debug_string,
                                                                                                time.time() - start_time))

        return OSMReadResult(nodes_dict=input_read_result.nodes_dict, ways_dict=input_read_result.ways_dict,
                            relations_dict=relations_dict, rel_nodes_dict=rel_nodes_dict, rel_ways_dict=rel_ways_dict)


    def fetch_osm_db_data_relations_places(self, input_read_result: OSMReadResult) -> OSMReadResult:
        first_part = "((r.tags @> 'type=>multipolygon' OR r.tags @> 'type=>boundary')"
        first_part += " AND (r.tags @> 'place=>city' OR r.tags @> 'place=>town'))"
        return self.fetch_osm_db_data_relations_keys(input_read_result, first_part, 'places')


    def fetch_osm_db_data_relations_buildings(self, input_read_result: OSMReadResult) -> OSMReadResult:
        first_part = "((r.tags @> 'type=>multipolygon'"
        first_part += " AND " + self.construct_tags_query(["building", "building:part"], list(), "r")
        first_part += ") OR r.tags @> 'type=>building')"
        return self.fetch_osm_db_data_relations_keys(input_read_result, first_part, 'buildings')


    def fetch_osm_db_data_relations_riverbanks(self, input_read_result: OSMReadResult) -> OSMReadResult:
        first_part = "(r.tags @> 'type=>multipolygon'"
        first_part += " AND r.tags @> 'waterway=>riverbank')"
        return self.fetch_osm_db_data_relations_keys(input_read_result, first_part, 'riverbanks')


    def fetch_osm_db_data_relations_routes(self, input_read_result: OSMReadResult) -> OSMReadResult:
        first_part = "(r.tags @> 'type=>route'"
        first_part += " AND r.tags @> 'route=>ferry')"
        return self.fetch_osm_db_data_relations_keys(input_read_result, first_part, 'ferry_route')


    def make_db_connection(self):
        """"Create connection to the database based on parameters."""
        connection = psycopg2.connect(database=parameters.DB_NAME, host=parameters.DB_HOST, port=parameters.DB_PORT,
                                    user=parameters.DB_USER, password=parameters.DB_USER_PASSWORD)
        return connection


    def construct_intersect_bbox_query(self, is_way: bool = True) -> str:
        """Constructs the part of a sql where clause, which constrains to bounding box."""
        query_part = "ST_Intersects("
        if is_way:
            query_part += "w.bbox"
        else:
            query_part += "n.geom"
        query_part += ", ST_SetSRID(ST_MakeBox2D(ST_Point({}, {}), ST_Point({}, {})), 4326))"
        return query_part.format(parameters.BOUNDARY_WEST, parameters.BOUNDARY_SOUTH,
                                parameters.BOUNDARY_EAST, parameters.BOUNDARY_NORTH)


    def create_key_value_pair(key: str, value: str) -> str:
        """A string combining a key and a value for query in PostGIS"""
        return '{}=>{}'.format(key, value)
    
    def construct_tags_query(self, req_tag_keys: List[str], req_tag_key_values: List[str], table_alias: str = "w") -> str:
        """Constructs the part of a sql WHERE clause, which constrains the result based on required tag keys.
        In req_tag_keys at least one of the key needs to be present in the tags of a given record.
        In req_tag_key_values at least one key/value pair must be present (e.g. 'railway=>platform') - the key
        must be separated without blanks from the value by a '=>'."""
        tags_query = ""
        if len(req_tag_keys) == 1:
            tags_query += table_alias + ".tags ? '" + req_tag_keys[0] + "'"
        elif len(req_tag_keys) > 1:
            is_first = True
            tags_query += table_alias + ".tags ?| ARRAY["
            for key in req_tag_keys:
                if is_first:
                    is_first = False
                else:
                    tags_query += ", "
                tags_query += "'" + key + "'"
            tags_query += "]"

        if req_tag_key_values:
            if tags_query:
                tags_query += " AND "
            if len(req_tag_key_values) == 1:
                tags_query += table_alias + ".tags @> '" + req_tag_key_values[0] + "'"
            else:
                tags_query += "("
                is_first = True
                for key_value in req_tag_key_values:
                    if is_first:
                        is_first = False
                    else:
                        tags_query += " OR "
                    tags_query += table_alias + ".tags @> '" + key_value + "'"
                tags_query += ")"

        return tags_query
    
    def fetch_db_way_data(self, req_way_keys: List[str], req_way_key_values: List[str], db_connection) -> Dict[int, Way]:
        """Fetches Way objects out of database given required tag keys and boundary in parameters."""
        query = """SELECT id, tags, nodes
        FROM ways AS w
        WHERE
        """
        query += self.construct_tags_query(req_way_keys, req_way_key_values)
        query += " AND "
        query += self.construct_intersect_bbox_query()
        query += ";"

        result_tuples = self.fetch_all_query_into_tuple(query, db_connection)

        ways_dict = dict()
        for result in result_tuples:
            my_way = Way(result[0])
            my_way.tags = self._parse_hstore_tags(result[1], my_way.osm_id)
            my_way.refs = result[2]
            ways_dict[my_way.osm_id] = my_way

        return ways_dict

    def fetch_db_nodes_for_way(self, req_way_keys: List[str], req_way_key_values: List[str], db_connection) -> Dict[int, Node]:
        """Fetches Node objects for ways out of database given same constraints as for Way.
        Constraints for way: see fetch_db_way_data"""
        query = """SELECT n.id, ST_X(n.geom) as lon, ST_Y(n.geom) as lat
        FROM ways AS w, way_nodes AS r, nodes AS n
        WHERE
        r.way_id = w.id
        AND r.node_id = n.id
        AND """
        query += self.construct_tags_query(req_way_keys, req_way_key_values)
        query += " AND "
        query += self.construct_intersect_bbox_query()
        query += ";"

        result_tuples = self.fetch_all_query_into_tuple(query, db_connection)

        nodes_dict = dict()
        for result in result_tuples:
            my_node = Node(result[0], result[2], result[1])
            nodes_dict[my_node.osm_id] = my_node

        return nodes_dict

    def fetch_db_nodes_isolated(self, req_node_keys: List[str], req_node_key_values: List[tuple]) -> Dict[int, Node]:
        """Fetches Node objects isolated without relation to way etc."""
        req_node_key_values = [ "=>".join(kv) for kv in req_node_key_values ]

        start_time = time.time()

        db_connection = self.make_db_connection()

        query = """SELECT n.id, ST_X(n.geom) as lon, ST_Y(n.geom) as lat, n.tags
        FROM nodes AS n
        WHERE """
        query += self.construct_tags_query(req_node_keys, req_node_key_values, table_alias="n")
        query += " AND "
        query += self.construct_intersect_bbox_query(is_way=False)
        query += ";"

        result_tuples = self.fetch_all_query_into_tuple(query, db_connection)

        nodes_dict = dict()
        for result in result_tuples:
            my_node = Node(result[0], result[2], result[1])
            my_node.tags = self._parse_hstore_tags(result[3], my_node.osm_id)
            nodes_dict[my_node.osm_id] = my_node
        db_connection.close()

        used_list = req_node_key_values
        if len(req_node_keys) > 0:
            used_list = req_node_keys
        logging.info("Reading OSM node data for {0!s} from db took {1:.4f} seconds.".format(used_list,
                                                                                            time.time() - start_time))

        return nodes_dict
    
    def construct_tags_query(self, req_tag_keys: List[str], req_tag_key_values: List[str], table_alias: str = "w") -> str:
        """Constructs the part of a sql WHERE clause, which constrains the result based on required tag keys.
        In req_tag_keys at least one of the key needs to be present in the tags of a given record.
        In req_tag_key_values at least one key/value pair must be present (e.g. 'railway=>platform') - the key
        must be separated without blanks from the value by a '=>'."""
        tags_query = ""
        if len(req_tag_keys) == 1:
            tags_query += table_alias + ".tags ? '" + req_tag_keys[0] + "'"
        elif len(req_tag_keys) > 1:
            is_first = True
            tags_query += table_alias + ".tags ?| ARRAY["
            for key in req_tag_keys:
                if is_first:
                    is_first = False
                else:
                    tags_query += ", "
                tags_query += "'" + key + "'"
            tags_query += "]"

        if req_tag_key_values:
            if tags_query:
                tags_query += " AND "
            if len(req_tag_key_values) == 1:
                tags_query += table_alias + ".tags @> '" + req_tag_key_values[0] + "'"
            else:
                tags_query += "("
                is_first = True
                for key_value in req_tag_key_values:
                    if is_first:
                        is_first = False
                    else:
                        tags_query += " OR "
                    tags_query += table_alias + ".tags @> '" + key_value + "'"
                tags_query += ")"

        return tags_query

    def fetch_all_query_into_tuple(self, query: str, db_connection) -> List[Tuple]:
        """Given a query string and a db connection execute fetch all and return the result as a list of tuples"""
        cur = db_connection.cursor()
        logging.debug("Query string for execution in database: " + query)
        cur.execute(query)
        return cur.fetchall()

    def _parse_hstore_tags(self, tags_string: str, osm_id: int) -> Dict[str, str]:
        """Parses the content of a string representation of a PostGIS hstore content for tags.
        Returns a dict of key value pairs as string."""
        tags_dict = dict()
        if tags_string.strip():  # else we return the empty dict as is
            elements = tags_string.strip().split('", "')
            for element in elements:
                if element.strip():
                    subs = element.strip().split("=>")
                    if len(subs) in [2, 3] and len(subs[0].strip()) > 1:
                        key = subs[0].strip().strip('"')
                        if key not in tags_dict:
                            if len(subs) == 2 and len(subs[1].strip()) > 1:
                                tags_dict[key] = subs[1].strip().strip('"')
                            elif len(subs) == 3 and len((subs[1] + subs[2]).strip()) > 1:
                                tags_dict[key] = (subs[1] + '=>' + subs[2]).strip('"')
                            else:
                                msg = "hstore for osm_id={} has not valid key/value pair: '{}' in '{}'.".format(osm_id,
                                                                                                                subs,
                                                                                                                tags_string)
                                logging.warning(msg)

                        else:
                            message = "hstore for osm_id={} has same key twice: key={}, tags='{}'.".format(osm_id,
                                                                                                        key,
                                                                                                        tags_string)
                            logging.warning(message)
                    else:
                        message = "hstore for osm_id={} has not valid key/value pair: '{}' in '{}'.".format(osm_id,
                                                                                                            subs,
                                                                                                            tags_string)
                        logging.warning(message)
        return tags_dict
