import logging
from osm2city import parameters
from osm2city.utils.osm_db_pgprovider import OsmDBProvider
from osm2city.utils.osm_db_pgprovider import OSMDbPostgresProvider
from osm2city.utils.osm_db_overpass_provider import OsmDBOverpassProvider
class OsmDBProviderFactory(): 
    def getProvider() -> OsmDBProvider:
        if parameters.OVERPASS_URI != None:
           return OsmDBOverpassProvider()
        return OSMDbPostgresProvider()             
           
   
    