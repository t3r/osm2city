from typing import List
from osm2city.utils.osmparser import OSMReadResult

class OsmDBProvider():
    def fetch_osm_db_data_ways_key_values(req_key_values: List[tuple]) -> OSMReadResult:
        pass

    def fetch_osm_db_data_ways_keys(req_keys: List[str]) -> OSMReadResult:
        pass
        
    def fetch_osm_db_data_relations_places(input_read_result: OSMReadResult) -> OSMReadResult:
        pass

    def fetch_osm_db_data_relations_buildings(input_read_result: OSMReadResult) -> OSMReadResult:
        pass

    def fetch_osm_db_data_relations_riverbanks(input_read_result: OSMReadResult) -> OSMReadResult:
        pass

    def fetch_osm_db_data_relations_routes(input_read_result: OSMReadResult) -> OSMReadResult:
        pass

    def fetch_db_nodes_isolated(keys: list, key_value_pairs: List[tuple]) -> dict:
        pass

