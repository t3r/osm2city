from typing import List
import time
import logging
import overpass

from osm2city import parameters
from osm2city.utils.osmparser import OSMReadResult, Node, Way, Relation, Member

class OsmDBOverpassProvider():
    def fetch_osm_db_data_ways_key_values(self, req_key_values: List[str] ) -> OSMReadResult:
        return self.read_element_key_values('way', req_key_values )

    def fetch_osm_db_data_ways_keys(self, req_keys: List[str]) -> OSMReadResult:
        return self.read_element_keys('way', req_keys )
        
    def fetch_osm_db_data_relations_places(self, input_read_result: OSMReadResult) -> OSMReadResult:
        q = """
            relation["type"~"^multipolygon|boundary$"]["place"~"^city|town$"]({},{},{},{});
            (._;>;);
        """
        result = self.query( q )
        return OSMReadResult( 
            nodes_dict=input_read_result.nodes_dict,
            ways_dict=input_read_result.ways_dict, 
            rel_nodes_dict=result.rel_nodes_dict, 
            rel_ways_dict=result.rel_ways_dict,
            relations_dict=result.relations_dict
        )

    def fetch_osm_db_data_relations_buildings(self, input_read_result: OSMReadResult) -> OSMReadResult:
        q = """
            [out:json][bbox:{},{},{},{}];
            (
                way["building"];
                way["building:part"];
                relation["type"="multipolygon"]["building:part"];
                relation["type"="multipolygon"]["building"];
                relation["building"];
            );
            (._;>;);
            out;
        """
        return self.query( q, build=False )

    def fetch_osm_db_data_relations_riverbanks(self, input_read_result: OSMReadResult) -> OSMReadResult:
        q = """
            nwr["type"="multipolygon"]["waterway"="riverbank"]({},{},{},{});
            (._;>;);
        """

        return self.query( q )

    def fetch_osm_db_data_relations_routes(self, input_read_result: OSMReadResult) -> OSMReadResult:
        q = """
            nwr["route"="ferry"]["type"="router"]({},{},{},{});
            (._;>;);
        """

        return self.query( q )
    
    def fetch_db_nodes_isolated(self, keys: list, key_value_pairs: List[tuple]) -> dict:
        # at least one of the keys AND one of the key_value_pairs must be present if both lists are set
        # otherwise at least on item of the used list must be present
        if len(keys) > 0 and len(key_value_pairs) > 0:
            raise Exception("using both, key and kv is not implemented")

        if len(keys) > 0:
            return self.read_element_keys( 'node', keys ).nodes_dict
        
        if len(key_value_pairs) > 0:
            return self.read_element_key_values( 'node', key_value_pairs ).nodes_dict
        
        raise Exception("using neither key nor kv is not implemented")



    def read_element_keys(self, element,keys) -> OSMReadResult:
        q = """
            [out:json][bbox:{},{},{},{}];
            (
        """

        for k in keys:
            q += """{}["{}"];""".format(element,k)

        q += """ 
            );
            (._;>;);
            out;
        """
        return self.query( q, False )

    def read_element_key_values(self, element,key_value_pairs) -> OSMReadResult:
        q = """
            [out:json][bbox:{},{},{},{}];(
        """
        for kv in key_value_pairs:
            q += """{}["{}"="{}"];""".format(element,kv[0],kv[1])
        q += """
            );
            (._;>;);
            out;
        """
        return self.query( q, False )
    
    def query(self, q, build=True) -> OSMReadResult:
        headers = { "Accept-Charset": "utf-8;q=0.7,*;q=0.7", "User-Agent": "FlightGear-OSM2CITY/0.0.1"}
        api = overpass.API(endpoint=parameters.OVERPASS_URI, headers=headers)
        start_time = time.time()
        q = q.strip().format(parameters.BOUNDARY_SOUTH, parameters.BOUNDARY_WEST, parameters.BOUNDARY_NORTH, parameters.BOUNDARY_EAST)
        logging.debug("Sending overpass query '{}'".format( q ))

        #response = api.get(q,responseformat="json", build=build)
        max_attempts = 3
        delay_seconds = 10
        for attempt in range(max_attempts):
            try:
                response = api.get(q, responseformat="json", build=build)
                break  # Exit the loop if the request is successful
            except Exception as e:
                if attempt == max_attempts - 1:
                    # If this is the last attempt, raise the exception
                    raise e
                else:
                    logging.warning(f"An exception occurred while connecting to overpass: {e}. Retrying in {delay_seconds} seconds...")
                    time.sleep(delay_seconds)
                    delay_seconds *= 1.5

        elements = response['elements']
        logging.info("overpass query '{0!s}' took {1:.4f} seconds and returned {2:d} entries".format( q, time.time() - start_time, len(elements) ))

        w_dict = { id: w for w,id in (way_from_json(w) for w in filter( lambda e: e['type'] ==  'way', elements )) }
        n_dict = { id: n for n,id in (node_from_json(n) for n in filter( lambda e: e['type'] == 'node', elements )) }
        r_dict = { id: r for r,id in (relation_from_json(r) for r in filter( lambda e: e['type'] == 'relation', elements )) }

        result =  OSMReadResult(nodes_dict=n_dict, ways_dict=w_dict,
                                relations_dict=r_dict, rel_nodes_dict=n_dict, rel_ways_dict=w_dict)
        
        return result
    
    

def node_from_json( json ):
    return Node(json['id'], json['lat'], json['lon'])

def way_from_json( json ):
    way = Way( json['id'] )
    if 'tags' in json:
        way.tags = json['tags']
    way.refs = json['nodes']
    return way, way.osm_id

def node_from_json( json ):
    node = Node(json['id'], json['lat'], json['lon'])
    if 'tags' in json:
        node.tags = json['tags']
    return node, node.osm_id

def member_from_json( json ):
    return Member(json['ref'],json['type'],json['role'])

def relation_from_json( json ):
    rel = Relation(json['id'])
    if 'tags' in json:
        rel.tags = json['tags']
    if 'members' in json:
        for m in json['members']:
            rel.add_member( member_from_json(m) )
    return rel, rel.osm_id