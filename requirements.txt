# descartes also installs amongst others numpy, matplotlib, pillow
descartes==1.1.0
shapely==1.7.1
networkx==2.5
requests==2.24.0
pyproj
scipy
overpass
psycopg2
